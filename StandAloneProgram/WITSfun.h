#ifndef WITSfun_h
#define WITSfun_h
#include "Arduino.h"
#include <SD.h>

#define M 8 //row dimension for the deglim array
#define N 4 //col dimension for deglim array

const double pi = 3.14159;

//values to convert accel(V) to accel(g)
const double xm = 1.133;
const double xb = -2.10;
const double zm = 1.1457;
const double zb = -1.754;

// initialize functions

void conservPWR (unsigned long ) ;
void printArray (double *, byte ) ;
void print2Darry (double [][N]) ;
void singleDAQ (byte &, int , boolean ) ;
void deltaLED(byte &, byte &, const byte &) ;
void selectFileName(File , int , char *) ;
void printDirectory(File , int ) ;
int RXnum() ;
double compAng(double , double ) ;
byte deg2sensor (double &, double *, boolean , byte &) ;
int inputButton(int , unsigned long) ;
void RDWRdata(double *, int &, byte &, byte &, char ) ;
void deg2sensor (byte &, double &, double [][N], boolean , byte &) ; 
void Rng2Limits(double *, double [][N], int &) ; 

#endif // WITSfun_h
