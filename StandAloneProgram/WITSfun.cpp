#include "WITSfun.h"

void conservPWR (unsigned long interval) {
  while(1) {
    Serial.println("Reset/turn off WITS to conserve power!");
    delay(interval);
  }//end while
};//end conservPWR
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void printArray (double *pArray, byte numArray) {//send a pointer array to Serial
  // Serial.println("Contents of saved array:");
  for (uint8_t i = 0; i < numArray; i++) {
    Serial.print("\t");
    Serial.println(*(pArray + i));
  }//end for loop
};//end printArray
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void print2Darry (double pArray[][N]) {
  //this is just as fast as pulling directly above
   Serial.println("Contents of saved array:");
  for (uint8_t i = 0; i < M; i++) {
    Serial.print("{ ");
    for (uint8_t j = 0; j < N; j++) {

    Serial.print(pArray[i][j]);
    Serial.print(" ");
    }//end inner for loop
    Serial.println("} ");
  }//end for loop
};//end printArray
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void singleDAQ (byte &choice, int ctrlPin, boolean Hardbutton) {
// Function used to either listen for a string command or count digital
// button presses from the commander XBee module.
  
  Serial.print("Which sensor?...");
  if(Hardbutton) {
    int intTemp;
    
    intTemp = inputButton(ctrlPin,4000);
    choice = (byte&) intTemp; 
  }
  else {
  while(!Serial.available());
  delay(30);
  choice = Serial.read()-'0';
  }

  if(choice >= 0 && choice <=8) {
    Serial.print("  sensor ");
    Serial.print(choice);
    Serial.println(" was chosen! Prog. will commence...");
  }
  else {
    Serial.println("Invalid sensor! Please choose a sensor between # 1-8 ...");
    singleDAQ(choice, ctrlPin, Hardbutton);
  }//end else
  choice--;
};//end singleDAQ

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void deltaLED(byte &element, byte &previous, const byte &iredsNum) {
  //Function to handle changing between LEDs
  if (element == 0) {   //through the 'pass' permission given in the previous statements
    if (iredsNum == 1) {
      previous = 0;
      element = 0;
    }
    else {
      previous = element; 
      element++; 
    }
  }

  else if ( element == (iredsNum - 1) ) {
    previous = element;
    element--;
  }

  else if (element > previous) {
    previous = element;
    element++;
  }

  else  {
    previous = element;
    element--; 
  }
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void selectFileName(File dir, int selection, char* pfilename) {
  /***********************************************************************************
   * Function utilizies a given directory "/" to find the "selection" file and return 
   * it to a pointer char file name. Be sure to initilize a pointer char array.
   * e.g.:
   * char filename[] = "LOGFILEX.CSV";
   * char* pfilename = filename;          
   ************************************************************************************/
  int count = 0;
  while(count<=selection) {

    File entry =  dir.openNextFile();
    if (!entry) {
      // no more files
      //Serial.println("**nomorefiles**");
      break;
    }
    if (count == selection) {
      pfilename = entry.name();
      if (entry.isDirectory()) {
        Serial.println("/");
        selectFileName(entry, selection, pfilename);
      } 
    }
    count++;
  }
  dir.rewindDirectory();
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void printDirectory(File dir, int numTabs) {
  int counter = 0;
  while(true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      //Serial.println("**nomorefiles**");
      break;
    }
    for (uint8_t i=0; i<numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs+1);
    } 
    else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.print(entry.size(), DEC);
      Serial.print("\t");
      Serial.println(counter);
    }
    counter++;
  }
  dir.rewindDirectory();
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
int RXnum() {
  int avail = 0;
  float result = 0.0;
  int result_int = 0;
  //  Serial.print("Type file number of choice...");

  while(!Serial.available()) {

    avail = Serial.available();
    delay(30);
    avail = Serial.available();
  }

  float decade = pow(10, avail-1);

  while (avail != 0) {

    int temp = Serial.read() -'0';

    if (temp == 72) { //'x' was entered for cancellation
      result_int = 120;
      break;
    }
    result = temp*decade + result;
    decade = decade/10;
    avail--;
  }

  if (result_int != 120) {

    result_int = (int) result;

    if (result > 99.00) result_int++;

    Serial.print("Received file number ");
    Serial.println(result_int);
  }
  else {
    Serial.println("File retrieval cancelled.");
  }

  return result_int; 
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
double compAng(double axV, double azV) {

  double ax;
  double az;
  double deg;       

  //convert to accel in g
  ax = axV*xm+xb;
  az = azV*zm+zb;

  //edit if ax or az is beyond 1g or -1g  
  if (ax > 1 || ax < -1 ) {

    if(ax > 0)    ax = 1.0;
    
    else          ax = -1.0;
  }
  if (az > 1 || az < -1) {

    if(az > 0)    az = 1.0;
    
    else          az = -1.0;
    
  }

  deg = atan(az/ax);
  deg = deg*(180.0)/pi;

  if (ax < 0) { //case for 2nd and 3rd quad
               // case for 3rd quad (az < 0)
      deg = 180 + deg; 
    }

  else {// case for 1st and 4th quad
    if (az < 0) { // case for 4th quad
      deg = 360 + deg; 
    }
    //else it's in the 1st quad so we do nothing!
  }  
  return deg;
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
byte deg2sensor (double &deg, double *pdegArry, boolean checkIRED, byte &element) {
  byte accel_choice; 

  double vari1 = 22.5; //variation of the indicated angle 45/2
  double vari2 = 8;   //variation of the indicated angle 
  /*This program will indicate which sensor to use based on the degree measured by the
   accelerometer*/

  for (uint8_t i = 1; i < 9 ; i++) {
    //first determine which sensor to use
    if( deg >= ((*(pdegArry+i))-vari1) && deg <= ((*(pdegArry+i))+vari1) ) {  
      accel_choice = i;  
    }//end if

    if(checkIRED) {//if we're interested in specifying which IRED

        if ( deg >= ((*(pdegArry+i))-vari2) && deg <= ((*(pdegArry+i))+vari2) )  element = 1;

      else if ( deg > ((*(pdegArry+i))+vari2) ) element = 0;

      else if ( deg < ((*(pdegArry+i))-vari2) ) element = 2;

      else element = 1;
    }//end if     
  }//end for
  return accel_choice;
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
int inputButton (int inputPin, unsigned long timeLimit) {
// Function to collect button presses from the 'CMD' button and return the
// number of presses.
// Typically used if serial communication isn't registering the command
// chars to run a program

  int state = LOW;
  int lastState = LOW;
  unsigned long timer;
  int counterB = 0;

  Serial.print("Press CMD button repeatedly to represent your\nnumber choice");
  Serial.print(" ...you have ");
  Serial.print(timeLimit);
  Serial.println(" secs");

  timer = millis();
  while((millis() - timer) < timeLimit) {

    if (state == HIGH && lastState == LOW) {
      counterB++;
      Serial.print(counterB-1);
      Serial.print(".. ");
    }

    lastState = state;
    state = digitalRead(inputPin);
    delay(10);

  }

  counterB--; //I'm not sure why but the counter adds an extra number
  //so to compensate, I'm subtracting that number!            
  return counterB;
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void RDWRdata(double *pArray, int &iredCtrdeg, byte &intrvl, byte &countLimit, char decision) {
  /***********************************************************************
   * ---This manual is outdated but the idea is the same, review code
   * carefully ----------------------------------------------------------
   * This function will read data from the SD card "readFile" and save to 
   * a pointer array "pArray", or it can read data over serial and save that
   * data into readFile and pArray, depending on your desicion.
   * INPUT:    
   * 1) double pointer array: 
   * double Arry[num] = {0.0};
   * double *pointer_Arry = Arry;
   * 
   * 2) int pointer, this will indicate the measuring range of the IRED[1]
   * 
   * 3) char desicion 
   * 'r' lets you read file and save the array.
   * char of any other desicion will write and save the array.
   * OUTPUTS:
   * This will return the saved data into a pointer 
   * array and will write new data if asked to write.
   * 
   * EXAMPLES:
   * 
   * RDWRdata(myFile,pointer_Arry,'r'); 
   * => read from myFile, save data in pointer_Array
   * 
   * RDWRdata(myFile,pointer_Arry,'w'); 
   * => write data from Serial and save in myFile, while also 
   * saving to pointer_Array
   * 
   ************************************************************************/
  File readFile;
  char  pfile[] = "ACCLRNGE.TXT";

  int data;
  byte tempCL;
  float temp[]={
    0.0  };
  float temp_n = 0.0;
  int i = 0;
  int j = 0;

  if (decision == 'r') { //If we want to read data from SD card

    readFile = SD.open(pfile, O_READ);

    if (!readFile) {
      Serial.println("opening "+String(pfile)+" for read failed");
      return;
    }//end if

    while ((data = readFile.read()) > 0) {

      if (data == '\n' || data == '$' || data == '.') {

        if (data == '.') {//handle zeros past the decimal point
          while(data != '\n') data = readFile.read();
        }

        for(uint8_t k = 0; k<j; k++) {
          //use the temp array to compute the actual number 
          //that was read from the file
          temp_n = temp_n +temp[k]*pow(10,j - k - 1);
          temp[k] = 0.0;
          if (k == j - 1) {
            String ranstr = "blah"+String(i);
            //data isn't saved properly if I don't make this string
            //I'm not sure why
            delay(10);
            
            if ( i == 8) iredCtrdeg = temp_n; 
            
            else if (i == 9) {
              intrvl = (byte)temp_n;
              tempCL = 100 / ((byte)temp_n + 1);
              
              if ( (tempCL % ((byte)temp_n + 1)) > 0 )    countLimit = tempCL + 1;
              
              else countLimit = tempCL;
            }
            
            else  *(pArray+i) = temp_n; 
            
            j = 0;  
            i++;
            temp_n = 0.0;
          }//end inner if                
        }//end for loop
      }//end if
      else {
        if((data-'0') >= 0){ //make sure we don't save a decimal 
          temp[j] = data - '0';  //I could change how that's handled
          j++;                   //but decimal point accuracy isn't 
        }// end inner if     //necessary so I won't bother
      }// end else

    }//end while loop
  }//end major if
  else if (decision == 'w') { //case of when we'd like to write a file
    readFile = SD.open(pfile, O_CREAT | O_TRUNC | O_WRITE); 
    if (!readFile) {
      Serial.println("opening "+String(pfile)+" for write failed");
    }
    while(Serial.available()) {
      data = Serial.read();

      if (data == ' ' || data == ',' || data == ';') {

        for(uint8_t k = 0; k<j; k++) {

          temp_n = temp_n +temp[k]*pow(10,j - k - 1);
          temp[k] = 0.0;

          if (k == j - 1) {
            String ranstr = "blah"+String(i);
            //data isn't saved properly if I don't make this string
            //I'm not sure why
            delay(15);
            readFile.println(temp_n);
            
            if ( i == 8) iredCtrdeg = temp_n; 
            
            else if (i == 9) {
              intrvl = (byte)temp_n;
              tempCL = 100 / ((byte)temp_n + 1);
              
              if ( (tempCL % ((byte)temp_n + 1)) > 0 )    countLimit = tempCL + 1;
              
              else countLimit = tempCL;
            }
            
            else  *(pArray+i) = temp_n;  

            j = 0;  
            i++;
            temp_n = 0.0;
          }//end inner if                         
        }//end for loop
      }//end if
      else {
        if((data-'0') >= 0){ //make sure we don't save a decimal need to change how
          temp[j] = data - '0'; //that's handled
          j++;
        }// end inner if
      }// end else
      delay(15);
    }//end inner while loop
  }//end major else
  readFile.close();
};//end RDWRdata function
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void deg2sensor (byte &accChc, double &deg, double limit[][N], 
                                                boolean checkIRED, byte &element) { 
 //This program will indicate which sensor to use based on the degree 
 //measured by the accelerometer
 
  boolean deg_in_Rng = true;
  byte cntr = 0;

 
   while(deg_in_Rng) {// loop will compare sensor to given measuring ranges
            
      if (cntr == 3 & ( deg <= 360 & deg >= limit[cntr][3]
              | deg >= 0 & deg < limit[cntr][0] )) {
       //this special case if, is for the sensor that meaures between 350deg - 50deg         
        accChc = cntr;  //provide correct sensor to use       
        cntr =0; //reset counter i
        deg_in_Rng = false; // exit loop
              }
      //all other sensors should be comparable in this statement
      else if( deg >= limit[cntr][3] & deg < limit[cntr][0] ) {
        
        accChc = cntr;  //provide correct sensor to use    
        cntr = 0; //reset counter i
        deg_in_Rng = false; // exit loop             
      }// end else if
      else 
          cntr++;
  
     }//end while
        deg_in_Rng = true;//rest flag for while loop
  
      if(checkIRED) {//if we're interested in specifying which IRED
                         
          if ( deg >= limit[accChc][3]
                  & deg < limit[accChc][2])   {
                    
              element = 2;
                  }
  
          else if ( deg >= limit[accChc][2]
                  & deg < limit[accChc][1])  {
                    
              element = 1;
                  }
  
          else if ( deg >= limit[accChc][1]
                  & deg < limit[accChc][0]) {
                    
              element = 0;
                  }
          else if ( accChc == 3) {
              
              if ((deg >= limit[accChc][3] & deg < 360) 
                    | (deg >= 0 & deg < limit[accChc][2])) {
                  element = 2;
              }
          }
          else element = 1; 
  
      }  //end if checkIRED
};//} function
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void  Rng2Limits(double *_arry, double limit [][N], int &ctrIRED) {
// Lets organize the input array to what it should be 
// we are expecting an array of degress [1 -> 8] 
// but I'll change the order to [4->8 ,1->3]
// initialize
  byte const cells = 8;
  double temp = 0.0;
  //temp cells
  double arrysr[cells]   = {0.0};
  double arrytmp[cells]  = {0.0};
  // cells used to organize vectors used to calculate the measuring range of sensors
  byte sort_ordr[cells]  = {3,4,5,6,7,0,1,2}; 
  byte low_ordr[cells]   = {5,6,7,0,1,2,3,4};
  byte hi_ordr[cells]    = {4,5,6,7,0,1,2,3};

  //first course of action is to sort the damn from small angle to large
  //this will result with having angles from sensors [4 -> 8 , 1 -> 3]
  for (uint8_t i = 0; i < cells; i++) {
    arrysr[i] = *(_arry + sort_ordr[i]);   
  }
  //find differences between angles  
  for (uint8_t i = 0; i < cells; i++ ) {
      
      if (i == 7) {
        temp = 360-arrysr[i];
        arrytmp[i] = abs(temp)+arrysr[0];
      }     
      
      else {
        temp = arrysr[i]-arrysr[i+1];
        arrytmp[i] = abs(temp);
      }
      arrytmp[i] = arrytmp[i]/2.0;       
      //represents the trailing measuring range of an IRED 
      arrytmp[i] = arrytmp[i] + arrysr[i];       
    
  }//end for loop
  //Will create matrix to use for limit
  for (uint8_t i = 0; i < cells; i++) {
    //leftist extreme between sensor borderlines (trailing end)
    limit[i][0] = arrytmp[low_ordr[i]];
    
    //border between 0 - > 1 ired
    limit[i][1] = *(_arry +i) + ctrIRED/2;
    
    //border between 1 - > 2 ired
    limit[i][2] = *(_arry +i) - ctrIRED/2;
    //rightest extreme between sensors borderlines (leading end)
    limit[i][3] = arrytmp[hi_ordr[i]];
  }//end for loop
};//end of function Rng2limits
