# WITS (Wireless Internal Tire Sensors) 

### V11a: July 2013

__Author__: Scott Naranjo

This repository should have all the information related to the WITS system
developed and described in [Naranjo's thesis](https://vtechworks.lib.vt.edu/bitstream/handle/10919/51165/Naranjo_SD_T_2013.pdf).

This is the latest version of the code and has been primarily untouched by me since 2013. With all the proper connections made, it should work out the box when you connect the XBEE transceiver to your computer USB to communicate over serial (9600 baudrate) and provide the Mega with power. If battery powered, the WITS XBEE will wake when the tilt switch is activated/tilted. 

This is my earliest, complicated C++ project and I originally kept it all on one large .ino file, this is stored in the _old_ folder. That version was likely complied with Arduino IDE v 1.0.2.
This current version is essentially the same but broken up into 3 files for better _'readability'_ and compiles with the lastest Arduino IDE v1.8.16 on MacOS. Viewing this in VS Code is easier on the eyes than the Arduino IDE.


## Sensor connections
Please refer to Appendix D (pg. 119) from the thesis for diagrams and pinouts of all devices and how they interconnect.

## Dependencies
* [__I2cMaster__](https://github.com/landis/arduino/tree/master/libraries/I2cMaster)
* __SoftRTClib__ - the latest _RTCLib_ would require changes to accomodate the specific RTC chip used; therefore the SoftRTClib is included with this repository.

## Ideal improvements
- Perform a new set of validation testing on the latest current configuration; as the initial test had too much variability in the sensor-rim attachments and inflation pressure. 
- The system can also benefit from Lithium-ion batteries which are lighter and can have a larger energy capacity than the current alkaline batteries used. In addition to using better batteries, a charger circuit could be attached to the wheel such that it can accept a power source without needing to remove the tire. 
- Other arrangements of the WITS optical sensors can be investigated to give more information of the contact patch, such as the deflection in the lateral direction.

