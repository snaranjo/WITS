#include <SD.h>
#include <I2cMaster.h>
#include <SoftRTClib.h>
#include <math.h>
/* This program appears to be working good. I added in this program the capability to edit
   in over serial the sample cycle per measurement. 
*/

#define ECHO_TO_SERIAL   0 // echo data to serial port
#define M 8 //row dimension for the deglim array
#define N 4 //col dimension for deglim array

/******************Initialize RTC use*********************/
#if defined(__AVR_ATmega1280__)\
|| defined(__AVR_ATmega2560__)
// Mega analog pins 4 and 5
// pins for DS1307 with software i2c on Mega
#define SDA_PIN 58
#define SCL_PIN 59

#else  // CPU type
#error unknown CPU
#endif  // CPU type

// An instance of class for software master
SoftI2cMaster i2c(SDA_PIN, SCL_PIN);

RTC_DS1307 RTC(&i2c);
/*****************************Initialize SD and RTC use****************************/

// for the data logging shield, we use digital pin 10 for the SD cs line
const int chipSelect = 10;

// the logging SdFile
File logfile;
// accel command SdFile
File accelfile;
// root directory
File root;

char CMDfile[] = "ACCLRNGE.TXT";
char filename[] = "WITS_000.CSV";

//pointers of filenames
char* pCMDfile = CMDfile;
char* pfilename = filename;

/*******************************Initialize Sensor Config**************************/
//initialization for measuring sharp sensors:
byte const sens = 8;
byte const iredsNum = 3;
byte Vout[sens]={
  12,13,14,15,8,9,10,11};    //Vout[curr_sens]put of IRED
byte Vin[sens]={
  29,37,45,53,52,44,36,28};   //Will provide power to selected IRED
byte IREDs[sens][iredsNum] = { 
 //2D array of IREDs[curr_sens] = [ <- 12.5, 0, -> 12.5] deg readings IREDs[curr_sens]
                      {23,25,27},        //Sensor1 IRED[i][0]          IRED[i][2]
                      {31,33,35},        //Sensor2          \ IRED[i][1]/
                      {39,41,43},        //Sensor3           \    |    /
                      {47,49,51},        //Sensor4            \   |   /
                      {46,48,50},        //Sensor5             \  |  /
                      {38,40,42},        //Sensor6              \ | /
                      {30,32,34},        //Sensor7    ___________\|/____________
                      {22,24,26}         //Sensor8   |(   deg ) SENSOR ( deg   )|
                    };                   //          |(<- 12.5)|center|(12.5 ->)| 
                                         //          |__________________________|
byte action = 0;
byte acc_choice = 1;
byte curr_sens = 0;
byte prev_sens = 0;

boolean MultiSens = true;

byte elem = 0;
byte prev = 0;

byte RDintrvl = 9; //Number of readings taken in smaller increments 
byte counter = 0;
byte Counter_limit = 10; //log data after readings/*10+10 points of data are recorded

float val;
float volt;
float convt = 5.0/1024; //Do simple arithmetic now instead of during every iteration

/***********************Initialize Accel Parameters*********************************/
double axV;
double ayV;
double azV;

double ax;
double az;
double deg;       //degRng is already defined incase of communication error for using file parameters
double degRng[8] = {247.5,292.5,337.5,22.5,67.5,112.5,157.5,202.5};

double *pdegRng = degRng;     //pointer for degRng i.e. degree ranges for accelerometer
double deglim[M][N] = {0.0};  //initialize matrix to organize sensor measurement ranges
int ctrIREDval = 20;          //value used to define the measuring space
                              //of the center IRED [1]
int editRng;

//values to convert accel(V) to accel(g)
double xm = 1.133;
double xb = -2.10;
double zm = 1.1457;
double zb = -1.754;

boolean accelIRED = false; //decision for if the code will determine which IRED to use
const double pi = 3.14;
byte slpPin = 7;
String accelmsg = "Ctrl via accelerometer ";
String settingmsg;
/********************Initializing timing variables***********************************/
/*Time between LED to turned on should be roughly 32 ms but because of the code
 found before the loop to collect data takes an average of 2.43 ms extra time 
 to perform, then I will subtract that amount from the 32 ms thus 29 ms will be used
 */
byte Sen_delay =28;           //Time between LED is turn and 1st reading taken
byte IR_delay = 17;            //Time between smaller increments of reading
byte minDelay = 5;             //Time between turning Vin[curr_sens] and IRED off||on
byte lpdelay = Sen_delay;

unsigned long curr_time = 0;   //records time just as data is being collected
unsigned long unixt = 1000;    //initialize const for unixtime (timestamp)
boolean RECunixt = true;       //'switch' for when initially logging unixt
unsigned long logTIME = 5000;  //timespan for logging unixt

unsigned long initLpspd;    //initialize loop speed count
int lpspd;                  //loop speed
int lpspd_diff;             //dDff lpspd and Sens_delay

int exeT;                   //Exection time for code in loop for collecting data
int itera_diff;             //Diff of exeT and Sens_delay
byte itera_limit = 6;       //Window of time to still collect good data i2n a loop

unsigned long intervalIndic = 25000; //interval to remind user that WITS is running
unsigned long initIndic = 0; //ref. time for notifiying the user that WITS is running
byte delaySerial = 30;

boolean requestButton = false;
int cmdPin = 2;
unsigned long initTime = 3000;
unsigned long SDtime = 10000;
int select;

//timing variable used in millis
extern volatile unsigned long timer0_millis; 

void setup(void)/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
{
  Serial.begin(9600);

  pinMode(cmdPin,INPUT);

  Serial.println("WITS Ready!");
  Serial.println("Choose either of the #'s:");
  Serial.println("(0) Collect from all sensors");
  Serial.println("(1) Collect from one sensor");
  Serial.println("( ) Accelerometer chooses sensors,\n"
    "( )  based on:\n"   
    "(2)   code\n"
    "(3)   code, specifying IRED\n"
    "(4)   ACCLRNGE.txt\n"
    "(5)   ACCLRNGE.txt, specifying IRED");
  Serial.println("(6) Read/Define Sensor angular positions ");
  Serial.println("(7) Read SD contents");

  //Note: The Indic is programmed to sleep for a period of 268 seconds, when it awakes,
  //the user will have 30 seconds to make a decision on what to do with the WITS system
  //after 30 seconds of being idle (no serial/RF activity) the Indic will go back to 
  //sleep. Continuing with the program will keep the Indic awake.
  while(!Serial.available()) {
    if (digitalRead(cmdPin)==HIGH) {
      int countz = 0;
      Serial.println("cmdPin detected..hold");
      unsigned long cmdTime = millis();
      while(millis()-cmdTime < 1000) {
        if(digitalRead(cmdPin)==HIGH) countz++;
      }

      if (countz > 50) {
        requestButton = true;
        break;
      }
      else {
        conservPWR(5000);
      }
    }
  }

  if(requestButton) {
    action = inputButton(cmdPin,initTime);
    if (action < 1) action = 0;
  }  
  else {
    delay(delaySerial);
    action = Serial.read()-'0';
  }

  /****************** initialize the SD card***********************/
  Serial.print("Initializing SD card...");

  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(chipSelect, OUTPUT);

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {

    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }

  Serial.println("card initialized.");
  /*************************Decision making*************************/

  if(action == 7) { //Print directory~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    root = SD.open("/");
    Serial.println("The following files are available to read:");
    printDirectory(root,0);  //Show files

    if (requestButton) {
      select = inputButton(cmdPin,SDtime)+1; 
    }
    else {
      Serial.println("Type in file number of choice or hit cancel(x).");

      select = RXnum();    //Retreive filename number of choice
    }

    if (select != 120) {// If you want to download a SdFile ( x => 172 (Dec) )
      //      int select = RXnum();    //Retreive filename number of choice

      selectFileName(root,select); //Retreive the actual filename
      Serial.print("File ");
      Serial.print(pfilename);
      Serial.println(" was selected and will be sent over Serial"); 

      root.close();
      //      Serial.println("SdFile will be sent over Serial...");
      // Proceed with dumping the SdFile selected 
      File dataFile = SD.open(pfilename);

      // if the file is available, write to it:
      if (dataFile) {
        while (dataFile.available()) {
          Serial.write(dataFile.read());
        }
        dataFile.close();
      }  
      // if the File isn't open, pop up an error:
      else {
        Serial.print("error opening ");
        Serial.println(pfilename);
      }
    }

    else {
      root.close(); //close root SdFile if you don't want to retrieve data
    }

    conservPWR(intervalIndic); //run program to remind user to reset power    
  }
  else if (action == 6) {//~~~~~~~ Read/Write Sensor angular positions ~~~~~~~~~~~\
   
    double file_arry[sens]={0.0};
    double *pfile_arry = file_arry;
    double newfile_arry[sens]={0.0};
    double *pnewfile_arry = newfile_arry;
    String accltitle = "-Sensor Accel Ranges-";
    String iredtitle = "-Measure range of Center IRED-";
    String cycletitle = "-Measurement cycle-";

    RDWRdata(pfile_arry,ctrIREDval,RDintrvl,Counter_limit,'r');//read file
    Serial.println("ACCLRNG.txt contains:");
    Serial.println(accltitle);
    printArray(pfile_arry,sens); //present saved array of pdegRng
    Serial.println(iredtitle);
    Serial.println("\t"+String(ctrIREDval));
    Serial.println(cycletitle);
    Serial.println("\t"+String(RDintrvl));
    
    if(!requestButton) {
    Serial.println("Would you like to edit? [y/n]");
    
    while(!Serial.available());
    delay(15);
    
    editRng = Serial.read() - 'y';
    }
    
    else       editRng = 1;

    if (editRng == 0) {
      
      Serial.println("Send new angular positions for sensors 1-8 over and \n"
                     "the measure range of the center IRED[1]. 9 in total\n"
                     "separate by ' ' or ',', and end with ';' ...");
      while(!Serial.available());
      RDWRdata(pnewfile_arry, ctrIREDval,RDintrvl,Counter_limit, 'w');               //write data
      Serial.println("WITS read your input as:");
      RDWRdata(pnewfile_arry, ctrIREDval,RDintrvl,Counter_limit,'r');              //read file again
      Serial.println(accltitle);
      printArray(pnewfile_arry,sens);            //present saved array of pdegRng
      Serial.println(iredtitle);
      Serial.println("\t"+String(ctrIREDval));
    }//end if editRng
    
    else pnewfile_arry = pfile_arry;
    
    Serial.println("Computed measurement ranges for sensors & IREDs:");
    Rng2Limits(pnewfile_arry, deglim, ctrIREDval);
    print2Darry (deglim);
    Serial.println("Measurement cycle = "+String(RDintrvl));

    Serial.println("The shown angular positions and center degree range\n"
                   "will be used to determine which sensors will be appointed\n"
                   "to measure deflection.");

    conservPWR(intervalIndic); //run program to remind user to reset power     
  }//end else if action == 6 

  else if (action == 5) {//~~~~~~~specify IRED & use  ACCLRNG.txt ~~~~~~~~~~~~~~~~~~
    RDWRdata(pdegRng,ctrIREDval,RDintrvl,Counter_limit,'r');
    settingmsg = accelmsg+"... specify IRED & use  ACCLRNG.txt";
    accelIRED = true;
  }//end else if action == 5

  else if (action == 4) {//~~~~~~~~~~~~~~~~~~based on ACCLRNG.txt~~~~~~~~~~~~~~~~~~~~
    RDWRdata(pdegRng,ctrIREDval,RDintrvl,Counter_limit, 'r');
    settingmsg = accelmsg+"... based on ACCLRNG.txt";
  }//end else if action == 4

  else if (action == 3) {//~~~~~~~~~~~~~~~~specify IRED & use code~~~~~~~~~~~~~~~~~~~
    settingmsg = accelmsg+".. specify IRED & use code-definded accel ranges";
    accelIRED = true;
  }//end else if action == 3

  else if (action == 2) {//~~~~~~~~~~~~~~~~~~~~~~~use code~~~~~~~~~~~~~~~~~~~~~~~~~~~
    settingmsg = accelmsg+"... via original code-defined accel ranges";
  }//end else if action == 2

  else if (action == 1) {//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    singleDAQ(curr_sens,requestButton);  //Collect data from one sensor
  }//end else if action == 1

  else {//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    MultiSens = true;
    settingmsg = "All sensors will be measured automatically!";
    
  }//end else 
  /**********************************************************************************/
/*%%%%%%%%%%%%%%%%%%%%  Create a new File to log   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

  boolean creatfile = true;
  uint8_t i = 0;

  while (creatfile) {

    pfilename[5] = i +'0';

    for (uint8_t j = 0; j < 100; j++) {

      pfilename[6] = j/10 + '0';
      pfilename[7] = j%10 + '0';
      if (!SD.exists(pfilename)) {
        // only open a new file if it doesn't exist
        logfile = SD.open(pfilename, O_CREAT | O_APPEND | O_WRITE);
        creatfile = false; 
        break;  // leave the loop!            
      }//end if
    }//end inner for loop
    i++;      
  }//end outter while loop

  if (!logfile) {
    Serial.println("couldnt create file!");
  }
  Serial.print("Logging to: ");
  Serial.println(pfilename); 
/*%%%%%%%%%%%%%%%%%%%%%%%%% End creating new logfile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

  if (!MultiSens){
    logfile.print("Sensor ");
    logfile.print(curr_sens);
    logfile.println(" has been chosen and gives control to the following pins");
    String VoltCTRL = "Vout = "+String(Vout[curr_sens-1])+";   Vin = "+String(Vin[curr_sens-1])+"; ";
    String IREDctrl = "IREDs = ["+String(IREDs[curr_sens-1][0])+" "+String(IREDs[curr_sens-1][1])+" "+String(IREDs[curr_sens-1][2])+"];";
    logfile.print(VoltCTRL);
    logfile.println(IREDctrl); 

#if ECHO_TO_SERIAL
    Serial.print("Sensor ");
    Serial.print(curr_sens+1);
    Serial.println(" has been chosen and gives control to the following pins");
    Serial.print(VoltCTRL);
    Serial.println(IREDctrl);   
#endif

  }//end of if MultiSens
  
  else { //case when using more than one sensor to read
    Serial.println(settingmsg);
    logfile.println(settingmsg);
  }

  String Title = "timestamp(s),time (ms),Sensor(V),Ax(v),Ay(V),Az(V),IRED,Sensor";
  logfile.println(Title);   

#if ECHO_TO_SERIAL
  Serial.println(Title); 
  Serial.println("__________________________________________________________");       
#endif 
  /******************************Initialize Accelerometer****************************/

  pinMode(slpPin,OUTPUT);
  digitalWrite(slpPin,HIGH);

    axV = analogRead(A2)*convt;
    azV = analogRead(A0)*convt;
  if (action > 1 ) { //start collecting data at the contact patch!
    
    deg = compAng(axV,azV);
    // determine the measuring range for all sensors - executed once
    Rng2Limits(pdegRng, deglim, ctrIREDval);
    // determine sensor pointed closet to the contact patch
    deg2sensor (curr_sens, deg, deglim, accelIRED, elem);
  }
  /***********************  SETUP for SHARP measuring  ******************************/
  // the array elements are numbered from 0 to (iredsNum - 1).
  // use a for loop to initialize each pin as an output:
  for (byte i = 0; i < sens; i++) {

    for (byte thisPin = 0; thisPin < iredsNum; thisPin++) {
      pinMode(IREDs[i][thisPin], OUTPUT);
      digitalWrite(IREDs[i][thisPin], LOW);
    }//end inner for loop

    pinMode(Vin[i], OUTPUT);  // Vin[curr_sens] to supply led power
    digitalWrite(Vin[i], LOW);
  }//end outer for loop
  /******************* Begin Data Collection Process ********************************/
  delay(minDelay);
  digitalWrite(Vin[curr_sens], HIGH);
  digitalWrite(IREDs[curr_sens][elem], HIGH);

  timer0_millis = 0; //will reset the millis() to 0 before loop
  //begins, otherwise, program will not start
  //if using the WAIT_TO_START debugging

  initLpspd = millis();
}

void loop(void)
{
  /* The essential flow of this algorithm is to take all the steps needed to collect data
   per IRED on "loop" through the void loop at a time. To elaborate, this program will:
   * Begin with IREDs[curr_sens] in off position, flip on the Vin[curr_sens] and IRED of choice*/

  lpspd = millis() - initLpspd;

  if (lpspd < Sen_delay) {      
    lpdelay = Sen_delay - lpspd;

  }
  else if (lpspd > Sen_delay) {
    lpspd_diff = lpspd - Sen_delay;

    if (lpspd_diff < IR_delay) {
      lpdelay = IR_delay - lpspd_diff;

    }
    else {  //assuming you got a bogus number. just play safe and reinitialize

        digitalWrite(IREDs[curr_sens][elem], LOW);
      digitalWrite(Vin[curr_sens], LOW);
      delay(minDelay);  
      digitalWrite(Vin[curr_sens], HIGH);
      digitalWrite(IREDs[curr_sens][elem], HIGH);
      lpdelay = Sen_delay;
    }
  }
  //  Serial.println(lpdelay);

  delay(lpdelay);
  //else condition would be if lpspd == Sen_delay, which doesn't require a delay!
  //thus no need to check it since all other conditions are checked upon before hand

  for (byte i = 0; i < RDintrvl; i++) {

    if (RECunixt) {
      if (millis() < logTIME) {          //only log timestamp for 7 seconds
        DateTime now = RTC.now();    //Fetch time
        unixt = now.unixtime();      //seconds since midnight 1/1/1970
      }

      else {// just give 0 so we can keep the column of the data File in order
        unixt = 0;   
        RECunixt = false;  
      }//end inner else
    }//end unixt if   

    curr_time = millis();             //Now that the data is fairly stable every other
    val = analogRead(Vout[curr_sens]);      //other 25 ms, we will collect a sample.
    volt = val*convt;
    axV = analogRead(A2)*convt;
    ayV = analogRead(A1)*convt;
    azV = analogRead(A0)*convt;

    logfile.print(unixt);
    logfile.print(",");
    logfile.print(curr_time);
    logfile.print(",");
    logfile.print(volt);
    logfile.print(",");
    logfile.print(axV);
    logfile.print(",");
    logfile.print(ayV);
    logfile.print(",");
    logfile.print(azV);
    logfile.print(",");
    logfile.print(elem);
    logfile.print(",");    
    logfile.println(curr_sens+1);

#if ECHO_TO_SERIAL   
    Serial.print(unixt);
    Serial.print(",");
    Serial.print(curr_time);
    Serial.print(",");
    Serial.print(volt);
    Serial.print(",");
    Serial.print(axV);
    Serial.print(",");
    Serial.print(ayV);
    Serial.print(",");
    Serial.print(azV);
    Serial.print(",");
    Serial.print(elem);
    Serial.print(",");    
    //    Serial.println(curr_sens);
    Serial.print(curr_sens+1);
    Serial.print(","); 
    Serial.print(Vout[curr_sens]);
    Serial.print(","); 
    Serial.print(Vin[curr_sens]);
    Serial.print(","); 
    Serial.println(IREDs[curr_sens][elem]);
#endif  //end ECHO_TO_SERIAL

    exeT = millis() - curr_time;

    if (exeT < IR_delay) {
      delay(IR_delay - exeT);  

    }//if
    else if (exeT > IR_delay) {
      itera_diff = exeT - IR_delay;

      if (itera_diff > itera_limit) {
        delay(itera_diff-itera_limit);

      } // inner if
      else {// This case we don't cause a delay because the execution time is just 
        // a little over the delay time, no big deal           
      }// inner else
    } //else if
  }//for loop

  //Now we turn off the IRED and Vin[curr_sens] and decided to switch to the next IRED
  digitalWrite(IREDs[curr_sens][elem], LOW);
  digitalWrite(Vin[curr_sens], LOW);

  if (MultiSens) {  //For cases when multiple sensors will be used for measurements

      if (action >= 2 && action <= 5) {//find position based on accelerometer degree position

      deg = compAng(axV,azV);
      deg2sensor (acc_choice, deg, deglim, accelIRED, elem);

      //only change sensors if we're out of range
      if (acc_choice != curr_sens) curr_sens = acc_choice;

    }//end if of changing degree 

      else if (elem == 0 && prev == 1) {
      //case for when sensors are being chosen randomly

      if (curr_sens > prev_sens) {//forward order of changing 
        prev_sens = curr_sens;
        if (curr_sens == sens-1)  curr_sens--;
        else                      curr_sens++;  

      }//end of inner if

      else {//reverse order of changing
        prev_sens = curr_sens;
        if (curr_sens == 0)  curr_sens++;             
        else                 curr_sens--;

      }//end inner else      

    }//end else if                                
  }//end if

  if (!accelIRED) {                  //If we're not relying on code to change IRED
    deltaLED(elem, prev, iredsNum);  //This function will cycle from IRED 1 [0] to 
    //IRED 5 [4] and go back in reverse order.
  }//end if action

  delay(minDelay);
  digitalWrite(IREDs[curr_sens][elem], HIGH);
  digitalWrite(Vin[curr_sens], HIGH); 
  initLpspd = millis();  //starting counting the loopspd to see that data is recorded
  counter++;             //at the best of times

  //Feedback for user to know that WITS is running...
  if ((millis()-initIndic) >= intervalIndic) {
    Serial.println("running..");
    initIndic = millis();
  }

  // Now we write data to disk! Don't sync too often - requires 2048 bytes of I/O to SD card
  // which uses a bunch of power and takes time

  if (counter > Counter_limit-1) {

    counter = 0;    
    //  Serial.println("Saving data...");
    logfile.close();
    logfile = SD.open(pfilename, O_CREAT | O_APPEND | O_WRITE);

  }//end if
}//end void loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~END VOID LOOP~~~~~~~~~~~~~~~~~~~~~~

void conservPWR (unsigned long interval) {
  while(1) {
    Serial.println("Reset/turn off WITS to conserve power!");
    delay(interval);
  }//end while
}//end conservPWR
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void printArray (double *pArray, byte numArray) {//send a pointer array to Serial
  // Serial.println("Contents of saved array:");
  for (uint8_t i = 0; i < numArray; i++) {
    Serial.print("\t");
    Serial.println(*(pArray + i));
  }//end for loop
}//end printArray
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void print2Darry (double pArray[][N]) {
  //this is just as fast as pulling directly above
   Serial.println("Contents of saved array:");
  for (uint8_t i = 0; i < M; i++) {
    Serial.print("{ ");
    for (uint8_t j = 0; j < N; j++) {

    Serial.print(pArray[i][j]);
    Serial.print(" ");
    }//end inner for loop
    Serial.println("} ");
  }//end for loop
}//end printArray
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void singleDAQ (byte &choice, boolean Hardbutton) {

  
  Serial.print("Which sensor?...");
  if(Hardbutton) {
    int intTemp;
    
    intTemp = inputButton(cmdPin,4000);
    choice = (byte&) intTemp; 
  }
  else {
  while(!Serial.available());
  delay(30);
  choice = Serial.read()-'0';
  }

  if(choice >= 0 && choice <=8) {
    Serial.print("  sensor ");
    Serial.print(choice);
    Serial.println(" was chosen! Prog. will commence...");
  }
  else {
    Serial.println("Invalid sensor! Please choose a sensor between # 1-8 ...");
    singleDAQ(choice,Hardbutton);
  }//end else
  choice--;
}//end singleDAQ

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void deltaLED(byte &element, byte &previous, const byte &iredsNum) {
  //Function to handle changing between LEDs
  if (element == 0) {   //through the 'pass' permission given in the previous statements
    if (iredsNum == 1) {
      previous = 0;
      element = 0;
    }
    else {
      previous = element; 
      element++; 
    }
  }

  else if ( element == (iredsNum - 1) ) {
    previous = element;
    element--;
  }

  else if (element > previous) {
    previous = element;
    element++;
  }

  else  {
    previous = element;
    element--; 
  }
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void selectFileName(File dir, int selection) {
  /***********************************************************************************
   * Function utilizies a given directory "/" to find the "selection" file and return 
   * it to a pointer char file name. Be sure to initilize a pointer char array.
   * e.g.:
   * char filename[] = "LOGFILEX.CSV";
   * char* pfilename = filename;          
   ************************************************************************************/
  int count = 0;
  while(count<=selection) {

    File entry =  dir.openNextFile();
    if (!entry) {
      // no more files
      //Serial.println("**nomorefiles**");
      break;
    }
    if (count == selection) {
      pfilename = entry.name();
      if (entry.isDirectory()) {
        Serial.println("/");
        selectFileName(entry,selection);
      } 
    }
    count++;
  }
  dir.rewindDirectory();
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void printDirectory(File dir, int numTabs) {
  int counter = 0;
  while(true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      //Serial.println("**nomorefiles**");
      break;
    }
    for (uint8_t i=0; i<numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs+1);
    } 
    else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.print(entry.size(), DEC);
      Serial.print("\t");
      Serial.println(counter);
    }
    counter++;
  }
  dir.rewindDirectory();
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
int RXnum() {
  int avail = 0;
  float result = 0.0;
  int result_int = 0;
  //  Serial.print("Type file number of choice...");

  while(!Serial.available()) {

    avail = Serial.available();
    delay(30);
    avail = Serial.available();
  }

  float decade = pow(10, avail-1);

  while (avail != 0) {

    int temp = Serial.read() -'0';

    if (temp == 72) { //'x' was entered for cancellation
      result_int = 120;
      break;
    }
    result = temp*decade + result;
    decade = decade/10;
    avail--;
  }

  if (result_int != 120) {

    result_int = (int) result;

    if (result > 99.00) result_int++;

    Serial.print("Received file number ");
    Serial.println(result_int);
  }
  else {
    Serial.println("File retrieval cancelled.");
  }

  return result_int; 
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
double compAng(double axV, double azV) {

  //convert to accel in g
  ax = axV*xm+xb;
  az = azV*zm+zb;

  //edit if ax or az is beyond 1g or -1g  
  if (ax > 1 || ax < -1 ) {

    if(ax > 0)    ax = 1.0;
    
    else          ax = -1.0;
  }
  if (az > 1 || az < -1) {

    if(az > 0)    az = 1.0;
    
    else          az = -1.0;
    
  }

  deg = atan(az/ax);
  deg = deg*(180.0)/pi;

  if (ax < 0) { //case for 2nd and 3rd quad
               // case for 3rd quad (az < 0)
      deg = 180 + deg; 
    }

  else {// case for 1st and 4th quad
    if (az < 0) { // case for 4th quad
      deg = 360 + deg; 
    }
    //else it's in the 1st quad so we do nothing!
  }  
  return deg;
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
byte deg2sensor (double &deg, double *pdegArry, boolean checkIRED, byte &element) {
  byte accel_choice; 

  double vari1 = 22.5; //variation of the indicated angle 45/2
  double vari2 = 8;   //variation of the indicated angle 
  /*This program will indicate which sensor to use based on the degree measured by the
   accelerometer*/

  for (uint8_t i = 1; i < 9 ; i++) {
    //first determine which sensor to use
    if( deg >= ((*(pdegArry+i))-vari1) && deg <= ((*(pdegArry+i))+vari1) ) {  
      accel_choice = i;  
    }//end if

    if(checkIRED) {//if we're interested in specifying which IRED

        if ( deg >= ((*(pdegArry+i))-vari2) && deg <= ((*(pdegArry+i))+vari2) )  element = 1;

      else if ( deg > ((*(pdegArry+i))+vari2) ) element = 0;

      else if ( deg < ((*(pdegArry+i))-vari2) ) element = 2;

      else element = 1;
    }//end if     
  }//end for
  return accel_choice;
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
int inputButton(int inputPin, unsigned long timeLimit) {

  int state = LOW;
  int lastState = LOW;
  unsigned long timer;
  int counterB = 0;

  Serial.print("Press CMD button repeatedly to represent your\nnumber choice");
  Serial.print(" ...you have ");
  Serial.print(timeLimit);
  Serial.println(" secs");

  timer = millis();
  while((millis() - timer) < timeLimit) {

    if (state == HIGH && lastState == LOW) {
      counterB++;
      Serial.print(counterB-1);
      Serial.print(".. ");
    }

    lastState = state;
    state = digitalRead(inputPin);
    delay(10);

  }

  counterB--; //I'm not sure why but the counter adds an extra number
  //so to compensate, I'm subtracting that number!            
  return counterB;
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void RDWRdata(double *pArray, int &iredCtrdeg, byte &intrvl, byte &countLimit, char decision) {
  /***********************************************************************
   * This function will read data from the SD card "readFile" and save to 
   * a pointer array "pArray", or it can read data over serial and save that
   * data into readFile and pArray, depending on your desicion.
   * INPUT:    
   * 1) double pointer array: 
   * double Arry[num] = {0.0};
   * double *pointer_Arry = Arry;
   * 
   *2) int pointer, this will indicate the measuring range of the IRED[1]
   * 
   * 3) char desicion 
   * 'r' lets you read file and save the array.
   * char of any other desicion will write and save the array.
   * OUTPUTS:
   * Although VOID, this will return the saved data into a pointer 
   * array and will write new data if asked to write.
   * 
   * EXAMPLES:
   * 
   * RDWRdata(myFile,pointer_Arry,'r'); 
   * => read from myFile, save data in pointer_Array
   * 
   * RDWRdata(myFile,pointer_Arry,'w'); 
   * => write data from Serial and save in myFile, while also 
   * saving to pointer_Array
   * 
   ************************************************************************/
  File readFile;
  char  pfile[] = "ACCLRNGE.TXT";

  int data;
  byte tempCL;
  float temp[]={
    0.0  };
  float temp_n = 0.0;
  int i = 0;
  int j = 0;

  if (decision == 'r') { //If we want to read data from SD card

    readFile = SD.open(pfile, O_READ);

    if (!readFile) {
      Serial.println("opening "+String(pfile)+" for read failed");
      return;
    }//end if

    while ((data = readFile.read()) > 0) {

      if (data == '\n' || data == '$' || data == '.') {

        if (data == '.') {//handle zeros past the decimal point
          while(data != '\n') data = readFile.read();
        }

        for(uint8_t k = 0; k<j; k++) {
          //use the temp array to compute the actual number 
          //that was read from the file
          temp_n = temp_n +temp[k]*pow(10,j - k - 1);
          temp[k] = 0.0;
          if (k == j - 1) {
            String ranstr = "blah"+String(i);
            //data isn't saved properly if I don't make this string
            //I'm not sure why
            delay(10);
            
            if ( i == 8) iredCtrdeg = temp_n; 
            
            else if (i == 9) {
              intrvl = (byte)temp_n;
              tempCL = 100 / ((byte)temp_n + 1);
              
              if ( (tempCL % ((byte)temp_n + 1)) > 0 )    countLimit = tempCL + 1;
              
              else countLimit = tempCL;
            }
            
            else  *(pArray+i) = temp_n; 
            
            j = 0;  
            i++;
            temp_n = 0.0;
          }//end inner if                
        }//end for loop
      }//end if
      else {
        if((data-'0') >= 0){ //make sure we don't save a decimal 
          temp[j] = data - '0';  //I could change how that's handled
          j++;                   //but decimal point accuracy isn't 
        }// end inner if     //necessary so I won't bother
      }// end else

    }//end while loop
  }//end major if
  else if (decision == 'w') { //case of when we'd like to write a file
    readFile = SD.open(pfile, O_CREAT | O_TRUNC | O_WRITE); 
    if (!readFile) {
      Serial.println("opening "+String(pfile)+" for write failed");
    }
    while(Serial.available()) {
      data = Serial.read();

      if (data == ' ' || data == ',' || data == ';') {

        for(uint8_t k = 0; k<j; k++) {

          temp_n = temp_n +temp[k]*pow(10,j - k - 1);
          temp[k] = 0.0;

          if (k == j - 1) {
            String ranstr = "blah"+String(i);
            //data isn't saved properly if I don't make this string
            //I'm not sure why
            delay(15);
            readFile.println(temp_n);
            
            if ( i == 8) iredCtrdeg = temp_n; 
            
            else if (i == 9) {
              intrvl = (byte)temp_n;
              tempCL = 100 / ((byte)temp_n + 1);
              
              if ( (tempCL % ((byte)temp_n + 1)) > 0 )    countLimit = tempCL + 1;
              
              else countLimit = tempCL;
            }
            
            else  *(pArray+i) = temp_n;  

            j = 0;  
            i++;
            temp_n = 0.0;
          }//end inner if                         
        }//end for loop
      }//end if
      else {
        if((data-'0') >= 0){ //make sure we don't save a decimal need to change how
          temp[j] = data - '0'; //that's handled
          j++;
        }// end inner if
      }// end else
      delay(15);
    }//end inner while loop
  }//end major else
  readFile.close();
}//end RDWRdata function
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void deg2sensor (byte &accChc, double &deg, double limit[][N], 
                                                boolean checkIRED, byte &element) { 
 //This program will indicate which sensor to use based on the degree 
 //measured by the accelerometer
 
  boolean deg_in_Rng = true;
  byte cntr = 0;

 
   while(deg_in_Rng) {// loop will compare sensor to given measuring ranges
            
      if (cntr == 3 & ( deg <= 360 & deg >= limit[cntr][3]
              | deg >= 0 & deg < limit[cntr][0] )) {
       //this special case if, is for the sensor that meaures between 350deg - 50deg         
        accChc = cntr;  //provide correct sensor to use       
        cntr =0; //reset counter i
        deg_in_Rng = false; // exit loop
              }
      //all other sensors should be comparable in this statement
      else if( deg >= limit[cntr][3] & deg < limit[cntr][0] ) {
        
        accChc = cntr;  //provide correct sensor to use    
        cntr = 0; //reset counter i
        deg_in_Rng = false; // exit loop             
      }// end else if
      else 
          cntr++;
  
     }//end while
        deg_in_Rng = true;//rest flag for while loop
  
      if(checkIRED) {//if we're interested in specifying which IRED
                         
          if ( deg >= limit[accChc][3]
                  & deg < limit[accChc][2])   {
                    
              element = 2;
                  }
  
          else if ( deg >= limit[accChc][2]
                  & deg < limit[accChc][1])  {
                    
              element = 1;
                  }
  
          else if ( deg >= limit[accChc][1]
                  & deg < limit[accChc][0]) {
                    
              element = 0;
                  }
          else if ( accChc == 3) {
              
              if ((deg >= limit[accChc][3] & deg < 360) 
                    | (deg >= 0 & deg < limit[accChc][2])) {
                  element = 2;
              }
          }
          else element = 1; 
  
      }  //end if checkIRED
}//} function
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void  Rng2Limits(double *_arry, double limit [][N], int &ctrIRED) {
// Lets organize the input array to what it should be 
// we are expecting an array of degress [1 -> 8] 
// but I'll change the order to [4->8 ,1->3]
// initialize
  byte const cells = 8;
  double temp = 0.0;
  //temp cells
  double arrysr[cells]   = {0.0};
  double arrytmp[cells]  = {0.0};
  // cells used to organize vectors used to calculate the measuring range of sensors
  byte sort_ordr[cells]  = {3,4,5,6,7,0,1,2}; 
  byte low_ordr[cells]   = {5,6,7,0,1,2,3,4};
  byte hi_ordr[cells]    = {4,5,6,7,0,1,2,3};

  //first course of action is to sort the damn from small angle to large
  //this will result with having angles from sensors [4 -> 8 , 1 -> 3]
  for (uint8_t i = 0; i < cells; i++) {
    arrysr[i] = *(_arry + sort_ordr[i]);   
  }
  //find differences between angles  
  for (uint8_t i = 0; i < cells; i++ ) {
      
      if (i == 7) {
        temp = 360-arrysr[i];
        arrytmp[i] = abs(temp)+arrysr[0];
      }     
      
      else {
        temp = arrysr[i]-arrysr[i+1];
        arrytmp[i] = abs(temp);
      }
      arrytmp[i] = arrytmp[i]/2.0;       
      //represents the trailing measuring range of an IRED 
      arrytmp[i] = arrytmp[i] + arrysr[i];       
    
  }//end for loop
  //Will create matrix to use for limit
  for (uint8_t i = 0; i < cells; i++) {
    //leftist extreme between sensor borderlines (trailing end)
    limit[i][0] = arrytmp[low_ordr[i]];
    
    //border between 0 - > 1 ired
    limit[i][1] = *(_arry +i) + ctrIRED/2;
    
    //border between 1 - > 2 ired
    limit[i][2] = *(_arry +i) - ctrIRED/2;
    //rightest extreme between sensors borderlines (leading end)
    limit[i][3] = arrytmp[hi_ordr[i]];
  }//end for loop
}//end of function Rng2limits
