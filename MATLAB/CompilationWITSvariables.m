%% Program to complie several variables into one
%  This program will simply compile separate tire variables into one easy
%  to use and comprehensive variable. The variables of interest are used
%  for deflected radius determination and are specific to the:
%  * 16 inch beadlock wheel
%  * current black-plastic-to-aluminum-stilts mount made for each sensor
%  * specific sensors used per location
%  * old calibration test results for sensor C
%  If any of those above are changed then this comprehensive will need an 
%  update. 
%% Load curent separate variables
folder = 'E:\Dropbox\MATLAB\';
file_a = 'senCpolyCoeffr2.mat';
sens = load(fullfile([folder 'WITS\'],file_a),'P');

file_b = 'Wheeldimensions.mat';
load(fullfile([folder 'variables\'],file_b),'Whl');

file_c = 'WhlrefBlackMountv1.mat';
load(fullfile([folder 'variables\'],file_c),'whlrf');

file_d = 'MockWheelStats.mat';
load(strcat([folder 'WITS\Accel\'],file_d),'v2g');
%% organize into one variable structure

whlref = struct();
whlref.text = {'avgDia - avg of diameter measurement for 16" beadlock rim(in)';...
               'stdDia - std of diameter measurement for 16" beadlock rim(in)';...
               'P_v2cm - poly. coeff. to convert distance data (V) to (cm)';...
               'P_ax_v2g - poly. coeff. to convert accl''r  ax (V) to (g)';...
               'P_az_v2g - poly. coeff. to convert accl''r  az (V) to (g)';...
               'rim_diff - distance from WITS face to end of wheel rim (in)'};
whlref.avgDia = Whl.dia_avg;
whlref.stdDia = Whl.dia_std;
whlref.P_v2cm = whlrf.Pcoeff;
whlref.Pax_v2g = v2g.Px;
whlref.Paz_v2g = v2g.Pz;
whlref.rim_diff = whlrf.rim_diff';
%% Saving struct whlref
save(strcat([folder 'variables\'],'CompiledWhlRefData_040213.mat'),...
     'whlref');


