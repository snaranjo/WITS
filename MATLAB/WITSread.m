function [s] = WITSread(folder,files,headerlines,save_accel)
%% -----------------------------------------------------------------------
%  Pull out data from text file and organize into a struct representing
%  a sensor and the fields of the struct saving information of the
%  following "Timestamp,time(ms),Timestamp+ms,Vdist,IRED,ax,ay,az". The
%  data in the file is expected to be in the following format:
%  "Timestamp, time(ms),Volt(distance),ax,ay,az,IRED,Sensor". Otherwise the
%  data will not be ogranized as required.
%               
% INPUT: folder    - string of folder location
%        files     - M x N cell array of file names, where,
%                      M = number of runs & N = # files per run
%                      example: 
%                  files = {'file1run1' 'file2run1' 'fileNrun1';...
%                          'file1runM' 'file2runM' 'fileNrunM'};
% 
%        headerlines - number of lines of text in the file
%                        before the data
%        save_accel - decide where to save accel data 
%        (0 - No || 1 - Yes)
% 
% OUTPUT: s, struct consisting:
%          date - timestamp in MM/DD/YYYY HH:MM:SS format
%          t    - time in s        |arbitrary timer adjusted to be in |
%          ts   - timestamp in s   |line with the recorded timestamp  |
%          v    - distance measured (V)
%          ired - indicates which IRED is providing the measurement
%          sens - indicates which sensor is providing the measurement
%          ax   - acceleration along the x-axis (V)
%          ay   - acceleration along the y-axis (V)
%          az   - acceleration along the z-axis (V)
%
% ver: 0.10
% Author: Scott Naranjo, scott.naranjo@gmail.com
% Last updated: 4/10/2013
%% ----------------------------------------------------------------------

%% Check Input

if nargin < 3 % handling the input arguments
    error('Not enough arguments!');
    
elseif nargin < 4 
        save_accel = 0;  
end

if isdir(folder)
    if folder(end) ~= '\' 
        folder = strcat(folder,'\');
    end
else
    msg = 'Invalid directory! - Input string of folder location\nExample : folder = ''C:\\folder_of_interest'';';
    error('msgid:randtext',msg);
    
end

if ~iscell(files)
    errstr = 'Invalid input for files.\nInput a 1 x N cell array of file names,where N = number of runs\nExample: files = {''filerun1'' ''filerun2'' ...''filerunN''};';
    error('msgid:randtext',errstr);
end

%% Organize data into structures 
if logical(save_accel) 
    s = struct('date',[],'t',[],'ts',[],'v',[],'ired',[],'sens',[],...
                'ax',[],'ay',[],'az',[]);
else
    s = struct('date',[],'t',[],'ts',[],'v',[],'ired',[],'sens',[]);
end
%% Code
file_size = size(files);

for i = 1 : file_size(1)
    for j = 1: file_size(2)
        
    completeFile = fullfile(folder,files{i,j});
    % Import raw data
    if logical(mean(files{i}(end-2:end) == 'csv')) %dechiper if data is .csv
        notCSV = false;
        %If machine does not have Excel, this block of code will spit
        %out a warning. So I just turn off warnings temporarily.
        warning('off','all');
        try
            A = struct('data',[]);
            A.data = xlsread(completeFile);%,headerlines,1);
        catch
            notCSV = true;
        end
        warning('off','all')
    else 
        notCSV = true;
    end
    
    if notCSV
        A = importdata(completeFile,',',headerlines); 
    %     disp('import done');
    end
    % put data in easier to work with arrays
    A.data(:,2)=A.data(:,2)./1000;  % convert from ms to seconds

    findNANs = find(isnan(A.data));

    if ~isempty(findNANs) 
        A.data(findNANs) = A.data(findNANs-1);
    end

    % A older WITS program didn't listed sensors as 0-7 rather than 1-8 so
    % this IF statement should fix that
    if max(A.data(:,8)) ~= 8 
        A.data(:,8) = A.data(:,8)+1;
    end
    % disp('struct initialized');
    % % Look for to find similar sensor entries
    % equal_sensor = find(A.data(1,8) == A.data(:,8));
    % % Verify if we're dealing with multiple sensors
    % multisense = length(equal_sensor) == length(A.data(:,8));

     if A.data(1,1) == 0 %instance when TS data is not collected

      s(i,j).t = A.data(:,2);
      s(i,j).ts = A.data(1,1);

     else

      %convert 1st UNIX timestamp to a month/day/year hh:mm:ss format 
      s(i,j).date = datestr(datenum([1970,1,1]) +...
                      A.data(1,1)/(24*3600));

      %find 1st sec change according to the timestamp
      one_sec = find(A.data(:,1) - A.data(1,1) == 1,1);

      %time diff from 1 sec timestamp and recorded abitrary timer
      tdiff = 1-(A.data(one_sec+1,2)-A.data(1,2));

      %adjust abitrary timer
      s(i,j).t = A.data(:,2) + tdiff ;

      %adjusting abitrary timer to be inline with recorded timestamp
      s(i,j).ts = s(i,1).t + A.data(1,1);
     end

      s(i,j).v = A.data(:,3);    %distance (V) for all IREDs
      s(i,j).ired = A.data(:,7); %which IRED ([0 1 2])
      s(i,j).sens = A.data(:,8); %which sensor([1 2 3 4 5 6 7 8])
    %    disp('save done');
      if logical(save_accel)
          s(i,j).ax = A.data(:,4);

          s(i,j).ay = A.data(:,5);

          s(i,j).az = A.data(:,6);
      end %of save_accel logic
    end % of sorting through all N files
end %of sorting through all M files    
clearvars -except s;
end
