This folder includes a demo script, data, and some functions that I used to convert the WITS voltage data to deflection/acceleration data.

Also included is a function for converting Terramechanics rig data into a data struct for easier data processing. 

Note: The voltage-distance calibration parameters for one sensor are assumed for all the sensors. Although each sensor calibration dataset cannot be too different from the other, ideally each sensor should have it's own calibration parameters to provide a more accurate interpretation.  
