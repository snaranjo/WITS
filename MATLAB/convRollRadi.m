function s = convRollRadi(data,tread)
%% convRollRadi
% Converts voltaga deflection data into rolling tire deflection data.
% Inputs:   data : M x N struct of data
%                  where M = number of runs, and
%                        N = number of files per run
%           Use the most up-to-date WITSread function to obtain the 
%           required input for data.
%           tread : measured tread thickness (cm). From inside->outside of 
%                   tire, ideally where WITS would be facing.
% 
% Outputs:  s, M x N struct of fields:
%                .dist  , distance of input Voltage data (cm)
%                .whlDef, total wheel deflection (inch)
%                .deg   , relative degree of WITS w.r.t the wheel center
%                         Used to determine if a WITS sensor is facing the
%                         contact patch. Only if input contains accel data.
% Author: Scott Naranjo, scott.naranjo@gmail.com
% Last updated: 4/3/2013
%% Check Input

if ~isstruct(data)
    errstr = 'Invalid input for data.\nInput a M x N struct of the data file, \n\twhere M = number of runs and N = number of files per run\nUse the most up-to-date WITSread function to obtain the required input for data.';
    error('msgid:randtext',errstr);
end
%% Code
% Initialization 
% Load necessary variables
s = struct();
convt = 2.54; %cm to 1 inch

persistent whlref;

if isempty(whlref) %~exists('whlref','var')
%     fldr = 'E:\Dropbox\MATLAB\variables\';
    fldr = [cd '\'];
    load(strcat(fldr,'CompiledWhlRefData_040213.mat'),...
     'whlref');
end

size_data = size(data);

%% Convert volt data to raw distance (cm)
    for i = 1:size_data(1)
        for j = 1:size_data(2)

         % Simply convert WITS deflection data (V) into distance data (cm)
            s(i,j).dist = polyval(whlref.P_v2cm,data(i,j).v); %(cm)
            
%             disp(['size of s(' num2str(i) ',' num2str(j) ').dist ' ...
%                     num2str(size(s(i,j).dist))]);
%             disp(['whlref.rimdiff size ' ...
%                 num2str(size(whlref.rim_diff(data(i,j).sens(:))'))]);

         % Determine the total wheel deflection - this includes the unique 
         % distance each sensor has with respect to the wheel, the wheel
         % radius and tread thickness
            s(i,j).whlDef = s(i,j).dist./convt+whlref.rim_diff(data(i,j).sens(:))'...
                +(whlref.avgDia/2) + tread/convt; %(inches)

            if isfield(data,'ax') && isfield(data,'ax') 
                   s(i,j).Deg = compAng([data(i,j).ax data(i,j).az]);
            end % of if exists ax and az
        end % of j for loop
    end % of i for loop
end % of function