function [d] = PullData(folder,file_array,reqvar)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Synopsis:        This program simply extracts data from a CELL ARRAY of
%                  text files. The structure can be 2D for instances when a
%                  large amount of data is required, say for multiple runs
%                  of particular data sets.
%   
%   Inputs:
%                   address:     string defining the location of the files
%                   (ex. folder = 'C:\Documents and Settings\Admin\MATLAB\';)
% 
%                   file_array:  cell array of file names (strings)
%                   (ex. file_arry = {'file1' 'file2''randomNamedfile3'};)
%                   NOTE: The program assumes you're inputing .txt files
%
%                   OPTIONAL
%                   reqvar:     cell array of field names of interest. Use
%                               fields that correlate EXACTLY as the
%                               output strcut. 
%                   (ex. reqvar = {'strpt' 'wspd'}; will output a struct 
%                    with time fields ('date' 'ts' 't') and ONLY the 
%                    requested fields of reqvar ('strpt 'wspd'))
%                             
%   Output:
%          d.date  : date with format -  day/month/year hr:min:sec
%          d.ts    : UNIX format timestamp (secs since 01/01/1970 0:00:00)
%         *d.t     : time vector for all data (s); frequency = 160 Hz
%          d.fx    : Fx force (N)
%          d.fy    : Fy force (N)
%          d.fz    : Fz force (N)
%          d.mx    : Mx moment (Nm)
%          d.my    : My moment (Nm)
%          d.mz    : Mz moment (Nm)
%          d.w_ang : wheel angle (deg)
%          d.strpt : stringpot (mm)    - do not trust this unit 
%          d.wspd  : wheel speed (rpm)
% 
%  *NOTE: LMS likes to give each set of data their own time vector; however
%  their shouldn't be any differences between the time vectors.  Working
%  with just one in the analysis should be just fine. 
%
% ver: 0.7
% Author: Scott Naranjo, scott.naranjo@gmail.com
% Last modified: 5/23/2013

%% CODE
%-----------------------initilization of structure-------------------------
% vars - field names used in the data struct
vars = {'date','ts','t','fx','fy','fz',...
        'mx','my','mz','ang','strpt','wspd'};
% indx - vector of the columns that correlate with the fields of interest
indx = [2 6 10 14 18 22 26 30 34];

% setup struct fields for data struction
structfields{numel(vars)*2} = [];  
for i = 1:numel(vars)
        structfields{2*i-1} = vars{i};
end

% handle case when there are requested variables
if exist('reqvar','var')
    reqarry{numel(reqvar)*2} = [];
    
    for i = 1:numel(reqvar)
         reqarry{2*i-1} = reqvar{i};
    end
    
    d = struct(structfields{1:6},reqarry{:});
else
    d = struct(structfields{:});
end
%--------------------------end of initialization---------------------------
names = fieldnames(d);
 
file_loc = strcat(folder,file_array,'.txt'); 
file_size = size(file_loc);
 
for i = 1 : file_size(1)
    %index i represents multiple runs of a particular scenario
    
    for j = 1 : file_size(2)
        %index j represents the actual scenario of interest
        %ex: d(x,y) => run x of y test 
        %    i.e. d(2,1) => 2nd Run of 1st slip condition test
        
        %********** Saving Timestamp********************
        FID = fopen(file_loc{i,j});
                
        fDATE = textscan(FID,'%s %*s %s %s %s %s %*[^\n]',23);

        k = 1;
        loopflag = 1;
        keyword = 'Created';
        %location of timestamp may differ, this loop finds it
        while(~isempty(loopflag))
        
            flag1 = logical(length(keyword) == ...
                            length(char(fDATE{1}(k))));

            if flag1
                flag2 = logical(keyword == char(fDATE{1}(k)));
                
                loopflag = find(flag2 == 0,1);
            end

            if ~isempty(loopflag)
                k = k+1;
            end
        end
        
        fclose(FID);            
        
        datestrg = [char(fDATE{3}(k)),'-',char(fDATE{2}(k)),'-',...
                    char(fDATE{4}(k)),' ',char(fDATE{5}(k))];
                
        d(i,j).date = datestrg;
        d(i,j).ts = int32(floor(86400 * (datenum(datestrg)...
                    - datenum('01-Jan-1970 00:00:00'))));      
        %********** End Saving Timestamp ********************

        A = importdata(file_loc{i,j},'\t',23);
        
        d(i,j).t = A.data(:,1);  
        
        for k = 1:numel(names)-3
            
            if exist('reqvar','var')
                x = find(strcmp(vars,reqvar{k}))-3;
            else
                x = k;
            end
        
            d(i,j).(names{k+3}) = A.data(:,indx(x));

        end

        clear A FID fDATE datestrg;
    end
end
clearvars -except d;

end

