%% open WITS data
clc; clear all; 

folder = [cd '\All LOW Cond\'];

% clear w;
wfiles = cell(3,2);
w_ind = [10 11; 13 12; 14 16; 15 15];

for i = 1:4
    for j = 1:2
        wfiles{i,j} = ['WITS_0' num2str(w_ind(i,j)) '.csv'];
    end
end

header = 1;
w = WITSread(folder,wfiles,header,1);
%% %% Converting deflection data to deflected radius graph
sw = convRollRadi(w,2); 
%% looking at deflection data after using convRollRadi
% col = {'b' 'r' 'g' 'k' 'y' 'm' 'c' 'b'};
col5 = {[0 0 0],[0 0 1],[0 .6 0],[1 0 0],... purple   yellow   gray  biege
    [.6 .6 .6],[.8 .6 .4],[.8 0 .8],[1 .4 .2]};
mrk = {'+' '.' '*' '+' '.' '*' '+' '.'};

sens = {'Sensor 1' 'Sensor 2' 'Sensor 3' 'Sensor 4' 'Sensor 5' 'Sensor 6',...
        'Sensor 7' 'Sensor 8'};
title_info = '0-5-10-15 slip All Low Cond. Run 1';    
h = zeros(8,3);
for j = 1:1
    
    for i = 1:1
       figure(i+20);
       hold on;
            for k = 1:max(w(i,j).sens)
                
                rnge = find(w(i,j).sens == k);
                
               h(k,i) = plot(w(i,j).t(rnge),sw(i,j).dist(rnge),...
                   'LineStyle','none','Marker',mrk{k},...
                   'MarkerEdgecolor',col5{k},...
                   'MarkerFaceColor',col5{k},'LineWidth',.5);
            end

        tit = title(title_info);
        leg = legend(h(:,i)', sens);
        xl = xlabel('Time (s)');
        yl = ylabel('Deflection measured (cm)');
%         set(leg,'Location','BestOutside');
        grid on;
        prop = [tit xl yl gca];
        set(prop,'Fontsize',18);
        set(leg,'Fontsize',13);
        hold off;
    end    
end