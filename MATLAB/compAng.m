function deg = compAng(accelarray) 
%% Program used to compute the angle as a result of two accelerations values
%  Input : accelarray = (N x 2 array) 
%                       where it should contain the following [ax az]
%                                     
%  Output : deg       = (1 D array) 
%                        the degree array given will represent the entire
%                        range of a circle i.e. 0-360 degrees
% Author: Scott Naranjo, scott.naranjo@gmail.com
% Last updated: 4/11/2013
%% Code  
persistent whlref;

if isempty(whlref) %~exists('whlref','var')
    %     fldr = 'E:\Dropbox\MATLAB\variables\';
    fldr = [cd '\'];
    load(strcat(fldr,'CompiledWhlRefData_040213.mat'),...
     'whlref');
end

  %convert to accel in g
  ax = polyval(whlref.Pax_v2g,accelarray(:,1));
  az = polyval(whlref.Paz_v2g,accelarray(:,2));
  
  %convert to degrees
  deg = atand(az./ax);
  
 for i = 1:length(deg) 
      if ax(i) < 0  %case for 2nd and 3rd quad

          if az(i) < 0  % case for 3rd quad
            deg(i) = 180 + deg(i); 

          else  % case for 2nd quad
            deg(i) = 180 + deg(i);
          end

      else % case for 1st and 4th quad
          if (az(i) < 0)  % case for 4th quad
            deg(i) = 360 + deg(i); 
          end
            %else it's in the 1st quad so we do nothing!
      end
 end
end
      
